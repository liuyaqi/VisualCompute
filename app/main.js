'use strict';
import 'jquery';
import 'materialize-css/bin/materialize.css!'
import 'materialize-css/bin/materialize.js'

import React from 'react';
import ReactDOM from 'react-dom';

import MainPage from "./VisualList";
import {BrowserRouter as Router} from 'react-router-dom';

ReactDOM.render(<Router><MainPage/></Router>, document.getElementById("app-container"));
