/**
 * Created by geo on 17/5/17.
 */
import './Styles/style.css!'

import React from 'react';
import {
    Switch,
    Route,
    Link    
} from 'react-router-dom';

import Sort from "./Component/Sort/Sort";
import BinarySortTree from "./Component/Tree/BinarySortTree";
import AlgoCard from "./Component/AlgoCard";
import Blank from "./Component/Blank";

class MainPage extends React.Component{
    render(){
        return (
            <div>
                <Link to="/">
                    <nav>
                        VisualCompute
                    </nav>
                </Link>
                <main>
                    <div className="row">
                        <PageSwitch/>
                    </div>
                </main>
            </div>
        );
    }
}

class PageSwitch extends React.Component{

    render(){
        return (
            <Switch>
                <Route path="/sort" component={Sort}></Route>
                <Route path="/tree" component={BinarySortTree}></Route>
                <Route path="/queue" component={Blank}></Route>
                <Route path="/find" component={Blank}></Route>
                <Route path="/" component={ComputeList}></Route>
                <Route component={ComputeList}></Route>
            </Switch>
        );
    }
}

class ComputeList extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        var itemsData = [
            {url:"/sort",title:"排序",info:"演示排序算法, 包括冒泡, 插入, 选择, 归并, 快速 等排序算法"},
            {url:"/tree",title:"二叉树",info:"演示二叉树的插入, 遍历过程"},
            {url:"/queue",title:"队列",info:"演示队列的创建"}];
        var data = itemsData.map((item)=>{
            return (
                <div className="item"  key={item.url}>
                    <AlgoCard  url={item.url} title={item.title}>{item.info}</AlgoCard>{/*标签中间对可以在主件中通过this.props.childern获取*/}
                </div>
            );
        });
        return (
            <div id="items" className="col s12 m12 l12">
                {data}
            </div>
        );
    }
}

export {MainPage as default};