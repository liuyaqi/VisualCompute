/**
 * Created by geo on 17/5/18.
 */
import React from 'react';

import {Link} from 'react-router-dom';

class AlgoCard extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="row">
                <div className="col s12 m6 s5">
                    <div className="card blue-grey darken-1">
                        <div className="card-content white-text">
                            <span className="card-title">{this.props.title}</span>
                            <p>{this.props.children}</p>
                        </div>
                        <div className="card-action">
                            <Link to={this.props.url} className="item">演示</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export {AlgoCard as default};